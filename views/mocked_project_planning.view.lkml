view: mocked_project_planning {
  sql_table_name: `poc.cm_project_planning_snapshot`;;

  dimension_group: act_e {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.act_e_date ;;
  }

  dimension_group: act_s {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.act_s_date ;;
  }

  dimension_group: act_duration {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${act_s_raw} ;;
    sql_end: ${act_e_raw} ;;
  }

  dimension_group: est_e {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.est_e_date ;;
  }

  dimension_group: diff_between_est_plan_e {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${est_e_raw} ;;
    sql_end: ${plan_e_raw} ;;
  }

  dimension_group: diff_between_est_act_e {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${est_e_raw} ;;
    sql_end: ${act_e_raw} ;;
  }

  dimension_group: est_s {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.est_s_date ;;
  }

  dimension_group: diff_between_est_plan_s {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${est_s_raw} ;;
    sql_end: ${plan_s_raw} ;;
  }

  dimension_group: diff_between_est_act_s {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${est_s_raw} ;;
    sql_end: ${act_s_raw} ;;
  }

  dimension_group: est_duration {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${est_s_raw} ;;
    sql_end: ${est_e_raw} ;;
  }

  dimension_group: plan_e {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.paln_e_date ;;
  }

  dimension_group: diff_between_plan_act_e {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${plan_e_raw} ;;
    sql_end: ${act_e_raw} ;;
  }

  dimension_group: plan_s {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.plan_s_date ;;
  }

  dimension_group: diff_between_plan_act_s {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${plan_s_raw} ;;
    sql_end: ${act_s_raw} ;;
  }

  dimension_group: plan_duration {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${plan_s_raw} ;;
    sql_end: ${plan_e_raw} ;;
  }

  dimension_group: integrated_s_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CASE
    WHEN act_s_date IS NOT NULL THEN act_s_date
    WHEN plan_s_date IS NOT NULL THEN plan_s_date
    ELSE
    est_s_date END ;;
  }

  dimension_group: integrated_e_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CASE
          WHEN act_e_date IS NOT NULL THEN act_e_date
          WHEN paln_e_date IS NOT NULL THEN paln_e_date
          ELSE
          est_e_date END ;;
  }

  dimension_group: integrated_duration {
    hidden: yes
    type: duration
    intervals: [day,week,month,year]
    sql_start: ${integrated_s_date_raw} ;;
    sql_end: ${integrated_e_date_raw} ;;
  }

  dimension: project_name {
    primary_key: yes
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: revenue {
    type: number
    value_format: "$#,##0.00"
    sql: ${TABLE}.revenue ;;
  }

  dimension: cm_status {
    label: "CM Status"
    type: string
    sql: ${TABLE}.cms ;;
  }

  measure: count {
    type: count
    drill_fields: [project_name]
  }

  measure: total_revenue {
    group_label: "revenue"
    type:  sum
    value_format: "$#,##0.00"
    sql: ${revenue} ;;
    drill_fields: [project_name]
  }

  measure: avg_revenue {
    group_label: "revenue"
    type:  average
    value_format: "$#,##0.00"
    sql: ${revenue} ;;
    drill_fields: [project_name]
  }

  measure: total_diff_days_between_est_plan_duration {
    group_label: "diff of project duration"
    type: sum
    sql: ${days_plan_duration} - ${days_est_duration};;
  }

  measure: total_diff_days_between_est_act_duration {
    group_label: "diff of project duration"
    type: sum
    sql: ${days_act_duration} - ${days_est_duration};;
  }

  measure: total_diff_days_between_plan_act_duration {
    group_label: "diff of project duration"
    type: sum
    sql: ${days_act_duration} - ${days_plan_duration};;
  }

  measure: total_act_duration_days {
    group_label: "project duration"
    type: sum
    sql: ${days_act_duration} ;;
  }

  measure: total_plan_duration_days {
    group_label: "project duration"
    type: sum
    sql: ${days_plan_duration} ;;
  }

  measure: total_est_duration_days {
    group_label: "project duration"
    type: sum
    sql: ${days_est_duration} ;;
  }

  measure: avg_duration_days {
    type: average
    value_format: "#,##0.00"
    sql: ${days_integrated_duration};;
  }

  measure: total_diff_days_between_plan_act_s {
    group_label: "diff between project start date"
    type: sum
    sql: ${days_diff_between_plan_act_s} ;;
  }

  measure: total_diff_days_between_plan_act_e {
    group_label: "diff between project end date"
    type: sum
    sql: ${days_diff_between_plan_act_e} ;;
  }

  measure: total_diff_days_between_est_act_s {
    group_label: "diff between project start date"
    type: sum
    sql: ${days_diff_between_est_act_s} ;;
  }

  measure: total_diff_days_between_est_plan_s {
    group_label: "diff between project start date"
    type: sum
    sql: ${days_diff_between_est_plan_s} ;;
  }

  measure: total_diff_days_between_est_act_e {
    group_label: "diff between project end date"
    type: sum
    sql: ${days_diff_between_est_act_e} ;;
  }

  measure: total_diff_days_between_est_plan_e {
    group_label: "diff between project end date"
    type: sum
    sql: ${days_diff_between_est_plan_e} ;;
  }
}
