view: project_owner {
  sql_table_name: `tw-rd-de-paul.poc.cm_project_owner`
    ;;

  dimension: owner {
    type: string
    sql: ${TABLE}.owner ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  measure: count {
    type: count
    drill_fields: [project_name]
  }

  measure: human_count {
    type:  count_distinct
    sql: ${owner} ;;
    drill_fields: [owner, project_name]
  }

  measure: project_count {
    type: count_distinct
    sql: ${project_name} ;;
    drill_fields: [owner, project_name]
  }
}
