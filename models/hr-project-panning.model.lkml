connection: "paul-poc-connection"

# include all the views
include: "/views/**/*.view"

datagroup: hr_project_panning_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: hr_project_panning_default_datagroup

explore: mocked_project_planning {
  join: project_owner {
    type: left_outer
    relationship: one_to_many
    sql_on: ${mocked_project_planning.project_name} = ${project_owner.project_name} ;;
  }
}
